#!/usr/bin/env python
import pathlib
import sys

import black


OUTPUT_SEPARATOR = '# output'


def main() -> None:
	if len(sys.argv) < 2:
		exit(1)
	example = pathlib.Path(__file__).parent / f'data/{sys.argv[1]}'
	if not example.is_file():
		exit(1)
	with example.open('r') as example_file:
		lines = example_file.readlines()
	src = ''.join(lines).strip() + '\n'
	out = black.format_str(src, mode = black.FileMode())
	with example.open('w') as example_file:
		for line in lines:
			example_file.write(line)
		example_file.write(f'\n\n{OUTPUT_SEPARATOR}\n\n\n')
		example_file.write(out)


if __name__ == '__main__':
	main()
