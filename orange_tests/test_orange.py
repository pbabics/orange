from typing import List, Tuple
import pathlib
import functools
import pytest

import black
from generate_test_example import OUTPUT_SEPARATOR


THIS_FILE = pathlib.Path(__file__)
THIS_DIR = THIS_FILE.parent
EMPTY_LINE = '# EMPTY LINE WITH WHITESPACE' + ' (this comment will be removed)'
TESTS = (THIS_DIR / 'data').iterdir()

format_string = functools.partial(
	black.format_str,
	mode = black.FileMode(target_versions = {black.TargetVersion.PY37, black.TargetVersion.PY38}),
)


def read_data(path: pathlib.Path) -> Tuple[str, str]:
	_input: List[str] = []
	_output: List[str] = []
	with path.open('r', encoding = 'utf8') as test:
		lines = test.readlines()
	result = _input
	for line in lines:
		line = line.replace(EMPTY_LINE, '')
		if line.rstrip() == OUTPUT_SEPARATOR:
			result = _output
			continue

		result.append(line)
	if _input and not _output:
		# If there's no output marker, treat the entire file as already pre-formatted.
		_output = _input[:]
	return ''.join(_input).strip() + '\n', ''.join(_output).strip() + '\n'


@pytest.mark.parametrize('path', TESTS)
def test_expected_output(path: pathlib.Path) -> None:
	source, expected = read_data(path)
	actual = format_string(source)
	assert actual == expected
	black.assert_equivalent(source, actual)
	black.assert_stable(source, actual, black.FileMode())
