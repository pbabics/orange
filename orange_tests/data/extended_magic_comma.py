from module import (
	x,
	y,
	z,
)
from module import (
	x,
	y,
	z
)
DICT = {
	"key1": "val1",
	"key2": "val2",
}
DICT = {
	"key1": "val1",
	"key2": "val2"
}
LIST = [
	1,
	2,
]
LIST = [
	1,
	2
]
SET = {
	1,
	2,
}
SET = {
	1,
	2
}
TUPLE = (
	1,
	2,
)
TUPLE = (
	1,
	2
)
def func(
	x,
	y,
):
	pass
def func(
	x,
	y
):
	pass
function_call(
	arg1,
	arg2,
)
function_call(
	arg1,
	arg2
)
def f(
	a,
	b,
	c,
) -> None:
	''' With return type, untyped, without default'''
	pass
def f(
	a,
	b,
	c = None,
) -> None:
	''' With return type, untyped, with default'''
	pass
def f(
	a: int,
	b: str,
	c: Optional[float],
) -> None:
	''' With return type, typed, without default'''
	pass
def f(
	a: int,
	b: str,
	c: Optional[float] = None,
) -> None:
	''' With return type, typed, with default'''
	pass


# output


from module import (
	x,
	y,
	z,
)
from module import x, y, z


DICT = {
	'key1': 'val1',
	'key2': 'val2',
}
DICT = {'key1': 'val1', 'key2': 'val2'}
LIST = [
	1,
	2,
]
LIST = [1, 2]
SET = {
	1,
	2,
}
SET = {1, 2}
TUPLE = (
	1,
	2,
)
TUPLE = (1, 2)


def func(
	x,
	y,
):
	pass


def func(x, y):
	pass


function_call(
	arg1,
	arg2,
)
function_call(arg1, arg2)


def f(
	a,
	b,
	c,
) -> None:
	''' With return type, untyped, without default'''
	pass


def f(
	a,
	b,
	c = None,
) -> None:
	''' With return type, untyped, with default'''
	pass


def f(
	a: int,
	b: str,
	c: Optional[float],
) -> None:
	''' With return type, typed, without default'''
	pass


def f(
	a: int,
	b: str,
	c: Optional[float] = None,
) -> None:
	''' With return type, typed, with default'''
	pass
