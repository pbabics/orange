s1 = 'just a normal string'
s2 = "this string has 'single quotes' in it"
s1 = "just a normal string"


# output


s1 = 'just a normal string'
s2 = "this string has 'single quotes' in it"
s1 = 'just a normal string'
